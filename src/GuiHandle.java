/**
 * This interface allows requests made by GUIs to be set or reset.
 * @author JJ
 */
public interface GuiHandle {

	/**
	 * Used by game engine to retrieve the visibility status of this JPanel.
	 * 
	 * @return The visibility status.
	 */
	public String getGUIRequest();

	/**
	 * Used by game engine to alter the visibility of this JPanel
	 * 
	 * @param request
	 *            The new visibility setting for this GUI.
	 */
	public void setGUIRequest(String request);
	
}
