/**
 * Constants used to define maze tile types. 
 * @author SBORG
 */
public enum Tile {
	
	UNVISITED(0), 
	PATH_TILE(1), 
	WALL_TILE(2), 
	ENTRY_TILE(3), 
	EXIT_TILE(4),
	OBSTACLE_TILE(5);

	private int tile;

	private Tile(int tile) {
		this.tile = tile;
	}

	public int getValue() {
		return tile;
	}

}
