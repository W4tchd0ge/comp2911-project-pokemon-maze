/**
 * Serves as a database for GUI requests and which GUI's are active.
 * @author JJ
 */
public class GuiStatus {

	// #########################################################
	// # 					CONSTRUCTOR                        #
	// #########################################################
	
	/**
	 * Instantiates a GuiStatus object is default to tell its parent GameEngine
	 * to display the MENU GUI.
	 */
	public GuiStatus() {
		guiRequest = Request.NONE.getName();
		activeGui = Request.MENU_GUI.getName();
	}

	// #########################################################
	// # 					PUBLIC METHODS					   #
	// #########################################################
	
	/**
	 * Get the current request from the GUIs.
	 * 
	 * @return A string containing the latest requests from the GUIs.
	 */
	public String getRequest() {
		return this.guiRequest;
	}

	/**
	 * Set the current requests from the GUIs.
	 * 
	 * @param req
	 *            A string containing the latest requests from the GUIs.
	 */
	public void setRequest(String req) {
		this.guiRequest = req;
	}

	/**
	 * Indicates which GUI is currently active.
	 * 
	 * @return A string which specifies which GUI is active.
	 */
	public String currentlyActive() {
		return this.activeGui;
	}

	/**
	 * Sets which GUI is currently Active.
	 * 
	 * @param active
	 *            A string which specifies which GUI is active.
	 */
	public void setCurrentlyActive(String active) {
		this.activeGui = active;
	}

	// #########################################################
	// # 					PRIVATE FIELDS 					   #
	// #########################################################

	private String guiRequest;
	private String activeGui;
	
}
