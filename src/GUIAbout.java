import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * GUI used for the about screen. 
 * @author SBORG
 */
@SuppressWarnings("serial")
public class GUIAbout extends JPanel implements GuiHandle {

	// #########################################################
	// #                      CONSTRUCTOR                      #
	// #########################################################
	
	/**
	 * Constructs an instance of GUI used to display about information. 
	 */
	public GUIAbout() {
		// ************************* PRIVATE FIELDS **************************//
		this.request = Request.NONE.getName();

		// ************************** GUI ELEMENTS ***************************//
		// BANNER
		JLabel bannerImg = new JLabel(new ImageIcon(
				GUIEndGame.class.getResource("/img/about-banner.png")));

		// BUTTON CONTAINER
		JPanel buttons = new JPanel();
		buttons.setBackground(new Color(230, 58, 0));
		GridBagLayout l_buttons = new GridBagLayout();
		l_buttons.columnWidths = new int[] { 1008 };
		l_buttons.rowHeights = new int[] { 40 };
		buttons.setLayout(l_buttons);

		// BACK BUTTON
		JButton playAgainButton = new JButton();
		playAgainButton.setIcon(new ImageIcon(GUIEndGame.class
				.getResource("/img/button-back.png")));
		playAgainButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setGUIRequest(Request.MENU_GUI.getName());
			}
		});
		
		// BUTTON SETTINGS
		playAgainButton.setFocusable(false);
		playAgainButton.setOpaque(false);
		playAgainButton.setContentAreaFilled(false);
		playAgainButton.setBorderPainted(false);
		playAgainButton.setFocusPainted(false);
		GridBagConstraints c_playButton = new GridBagConstraints();
		c_playButton.anchor = GridBagConstraints.PAGE_START;
		buttons.add(playAgainButton, c_playButton);

		// *************************** SUB-PANELS ****************************//
		// CONFIGURE GUI LAYOUT
		this.setBackground(new Color(74, 43, 42));
		this.setLayout(new GridBagLayout());
		GridBagLayout l_endGame = new GridBagLayout();
		l_endGame.columnWidths = new int[] { 1008 };
		l_endGame.rowHeights = new int[] { 16, 449, 50, 16 };
		this.setLayout(l_endGame);
		
		// ADD SUBCOMPONENTS TO GUI
		GridBagConstraints c_EndGameGUI = new GridBagConstraints();
		c_EndGameGUI.gridy = 1;
		this.add(bannerImg, c_EndGameGUI);
		c_EndGameGUI.gridy = 2;
		c_EndGameGUI.anchor = GridBagConstraints.PAGE_START;
		this.add(buttons, c_EndGameGUI);
	}

	// #########################################################
	// #                	PUBLIC METHODS                     #
	// #########################################################
	
	/**
	 * Used by game engine to retrieve the visibility status of this JPanel.
	 * 
	 * @return The visibility status. 
	 */
	public String getGUIRequest() {
		return request;
	}

	/**
	 * Used by game engine to alter the visibility of this JPanel.
	 * 
	 * @param request The new visibility setting for this GUI. 
	 */
	public void setGUIRequest(String request) {
		this.request = request;
	}
	
	// #########################################################
	// #                	PRIVATE FIELDS                     #
	// #########################################################
	
	private String request;
	
}
