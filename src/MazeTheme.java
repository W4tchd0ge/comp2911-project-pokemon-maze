/**
 * Constants used to define maze tiles used for different themes. 
 * @author SBORG
 */
public enum MazeTheme {
	
	FOREST_PATH("/img/maze-forest-path.png"),
	FOREST_WALL("/img/maze-forest-wall.png"),
	FOREST_ENTRY("/img/maze-forest-entry.png"),
	FOREST_EXIT("/img/maze-forest-exit.png"),
	FOREST_OBSTACLE("/img/maze-forest-obstacle.png"),

	CAVE_PATH("/img/maze-cave-path.png"),
	CAVE_WALL("/img/maze-cave-wall.png"),
	CAVE_ENTRY("/img/maze-cave-entry.png"),
	CAVE_EXIT("/img/maze-cave-exit.png"),
	CAVE_OBSTACLE("/img/maze-cave-obstacle.png"),
	
	GRASS_PATH("/img/maze-grass-path.png"),
	GRASS_WALL("/img/maze-grass-wall.png"),
	GRASS_ENTRY("/img/maze-grass-entry.png"),
	GRASS_EXIT("/img/maze-grass-exit.png"),
	GRASS_OBSTACLE("/img/maze-grass-obstacle.png"),
	
	WATER_PATH("/img/maze-water-path.png"),
	WATER_WALL("/img/maze-water-wall.png"),
	WATER_ENTRY("/img/maze-water-entry.png"),
	WATER_EXIT("/img/maze-water-exit.png"),
	WATER_OBSTACLE("/img/maze-water-obstacle.png");
	
	private String tile;

	private MazeTheme(String tile) {
		this.tile = tile;
	}

	public String getImage() {
		return this.tile;
	}
	
}
