import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * GUI used for the maze game.
 * @author SBORG
 */
@SuppressWarnings("serial")
public class GUIMaze extends JPanel implements GuiHandle {

	// #########################################################
	// #                      CONSTRUCTOR                      #
	// #########################################################

	/**
	 * Constructs an instance of the maze GUI.
	 * 
	 * @param maze
	 *            The specified maze to use for the game.
	 * @param player
	 *            The player name.
	 */
	public GUIMaze(JLayeredPane maze, String player) {
		// ************************* PRIVATE FIELDS **************************//
		this.request = Request.NONE.getName();
		this.mazeGame = maze;
		this.playerName = player;

		// *************************** STATUS PANE ***************************//
		// PLAYER NAME
		JLabel displayPlayerName = new JLabel(playerName);
		displayPlayerName.setFont(new Font("Arial Narrow", Font.BOLD, 16));
		displayPlayerName.setForeground(new Color(241,203,186));

		// RESTART BUTTON
		JButton restartButton = new JButton();
		restartButton.setIcon(new ImageIcon(GUIMaze.class
				.getResource("/img/button-restart.png")));
		restartButton.setFocusable(false);
		restartButton.setOpaque(false);
		restartButton.setContentAreaFilled(false);
		restartButton.setBorderPainted(false);
		restartButton.setFocusPainted(false);
		restartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int restartChoice = JOptionPane.showConfirmDialog(mazeGame,
						"Are you serious, mate?", "Restart Confirmation",
						JOptionPane.YES_NO_OPTION);
				if (restartChoice == JOptionPane.OK_OPTION) {
					setGUIRequest(Request.RESET.getName());
				}
			}
		});

		// MUSIC BUTTON
		JButton musicButton = new JButton();
		musicButton.setIcon(new ImageIcon(GUIMaze.class
				.getResource("/img/button-music.png")));
		musicButton.setFocusable(false);
		musicButton.setOpaque(false);
		musicButton.setContentAreaFilled(false);
		musicButton.setBorderPainted(false);
		musicButton.setFocusPainted(false);
		musicButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setGUIRequest(Request.MUTE.getName());
			}
		});

		// QUIT-TO-MENU BUTTON
		JButton quitButton = new JButton();
		quitButton.setIcon(new ImageIcon(GUIMaze.class
				.getResource("/img/button-menu.png")));
		quitButton.setFocusable(false);
		quitButton.setOpaque(false);
		quitButton.setContentAreaFilled(false);
		quitButton.setBorderPainted(false);
		quitButton.setFocusPainted(false);
		quitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int menuChoice = JOptionPane.showConfirmDialog(mazeGame,
						"Are you sure you want to return to the menu?",
						"Menu Return Confirmation", JOptionPane.YES_NO_OPTION);
				if (menuChoice == JOptionPane.OK_OPTION) {
					setGUIRequest(Request.MENU_GUI.getName());
				}
			}
		});

		// COMBINE BUTTONS INTO ONE PANEL
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		GridBagLayout l_buttonPanel = new GridBagLayout();
		l_buttonPanel.columnWidths = new int[] { 76, 25, 76 };
		buttonPanel.setLayout(l_buttonPanel);
		GridBagConstraints c_buttonPanel = new GridBagConstraints();

		c_buttonPanel.anchor = GridBagConstraints.CENTER;
		c_buttonPanel.gridx = 0;
		buttonPanel.add(restartButton, c_buttonPanel);

		c_buttonPanel.anchor = GridBagConstraints.CENTER;
		c_buttonPanel.gridx = 1;
		buttonPanel.add(musicButton, c_buttonPanel);

		c_buttonPanel.anchor = GridBagConstraints.CENTER;
		c_buttonPanel.gridx = 2;
		buttonPanel.add(quitButton, c_buttonPanel);

		// STATUS PANE CONTENT HOLDER
		JPanel statusPaneContents = new JPanel();
		GridBagConstraints c_contents = new GridBagConstraints();
		statusPaneContents.setOpaque(false);
		GridBagLayout l_contents = new GridBagLayout();
		l_contents.rowHeights = new int[] { 136, 32, 274, 0 };
		statusPaneContents.setLayout(l_contents);

		// ADD THE PLAYERS NAME
		c_contents.gridx = 0;
		c_contents.gridy = 1;
		statusPaneContents.add(displayPlayerName, c_contents);

		// ADD THE BUTTONS
		c_contents.gridx = 0;
		c_contents.gridy = 4;
		statusPaneContents.add(buttonPanel, c_contents);

		// PANE TO LAYER COMPONENTS
		JLayeredPane statusPane = new JLayeredPane();
		statusPane.setPreferredSize(new Dimension(256, 512));
		statusPane.setLayout(new GridBagLayout());

		// CREATE BACKGROUND IMAGE
		JLabel statusBackdrop = new JLabel(new ImageIcon(
				GUIMaze.class.getResource("/img/maze-sidepanel.png")));

		// ADD BACKGROUND AND STATUS CONTENT TO CONTAINER
		GridBagConstraints c_statusPane = new GridBagConstraints();
		c_statusPane.gridx = 0;
		c_statusPane.gridy = 0;
		statusPane.add(statusPaneContents, c_statusPane);
		statusPane.add(statusBackdrop, c_statusPane);

		// *************************** SUB-PANELS ****************************//
		// CONFIGURE GUI LAYOUT
		this.setBackground(new Color(74, 43, 42));
		GridBagLayout gbl_maze = new GridBagLayout();
		gbl_maze.columnWidths = new int[] { 768, 256 };
		gbl_maze.rowHeights = new int[] { 544 };
		this.setLayout(gbl_maze);

		// ADD MAZE AND STATUS PANE TO MAIN GUI
		GridBagConstraints c_maze = new GridBagConstraints();
		this.add(mazeGame);
		c_maze.gridx = 1;
		this.add(statusPane);
	}

	// #########################################################
	// # 					PUBLIC METHODS 					   #
	// #########################################################

	/**
	 * Used by game engine to retrieve the visibility status of this JPanel.
	 * 
	 * @return The visibility status.
	 */
	public String getGUIRequest() {
		return request;
	}
	
	/**
	 * Used by game engine to alter the visibility of this JPanel
	 * 
	 * @param request
	 *            The new visibility setting for this GUI.
	 */
	public void setGUIRequest(String request) {
		this.request = request;
	}

	/**
	 * Resets the maze game to it's initial state.
	 */
	public void resetMazeGame(JLayeredPane newMazeGame) {
		this.mazeGame = newMazeGame;
	}

	// #########################################################
	// # 					PRIVATE FIELDS 					   #
	// #########################################################

	private JLayeredPane mazeGame;
	private final String playerName;
	private String request;
	
}
