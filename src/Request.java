/**
 * Constants representing GUI requests.
 * @author JJ
 */
public enum Request {
	
	MENU_GUI("Menu"), 
	ABOUT_GUI("About"), 
	MAZE_GUI("Maze"), 
	END_GAME_GUI("End Game"), 
	RESET("Reset"), 
	MUTE("Mute"), 
	NONE("None");

	private String name;

	private Request(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
