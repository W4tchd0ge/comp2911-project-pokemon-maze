import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * Stores data related to the maze game to be displayed in the Maze GUI.
 * @author SBORG and Stefanus
 */
public class MazeData {

	// #########################################################
	// # 					CONSTRUCTOR 					   #
	// #########################################################
	
	/**
	 * Constructs an object to save information related to the maze game.
	 * 
	 * @param sprite
	 *            The player image that will use throughout the maze.
	 * @param difficulty
	 *            The maze difficulty level.
	 */
	public MazeData(String sprite, int difficulty) {
		this.mazeGen = new MazeGenerator();
		this.mazeData = mazeGen.generateMaze(difficulty);
		while (mazeGen.getContent(0, 0) == 0) {
			this.mazeData = mazeGen.generateMaze(difficulty);
		}
		this.spriteToUse = sprite;
		this.NUM_COL = mazeGen.getWidth();
		this.NUM_ROW = mazeGen.getHeight();
		this.mazeGame = makeMazeGame();
	}

	// #########################################################
	// # 					PUBLIC METHODS 					   #
	// #########################################################
	
	/**
	 * Retrieves the constructed maze game object from initial data.
	 * 
	 * @return The maze game.
	 */
	public JLayeredPane getOrigiMaze() {
		return mazeGame;
	}

	/**
	 * Pieces together maze background Tile with a moving player sprite.
	 * 
	 * @return Returns a JLayeredPane containing the fully functional maze game.
	 */
	public JLayeredPane makeMazeGame() {
		JPanel mazeTile = makeMazeTile();
		JLabel playerSprite = new JLabel(new ImageIcon(
				MazeData.class.getResource(spriteToUse)));

		// ADD KEYLISTENER TO MOVE SPRITE OVER THE MAZE
		mazeTile.setFocusable(true);
		addMotionSupport(playerSprite, mazeData, mazeGen.getEntryCol(),
				mazeGen.getEntryRow());

		// SET POSITIONING FOR PANEL ELEMENTS
		GridBagConstraints c_mazeContainer = new GridBagConstraints();
		c_mazeContainer.anchor = GridBagConstraints.LINE_START;
		c_mazeContainer.gridx = mazeGen.getEntryRow();
		c_mazeContainer.gridy = mazeGen.getEntryCol();

		// ADD PLAYER SPRITE AND MAZE Tile TO MAZE PANEL
		JLayeredPane mazeContainer = new JLayeredPane();
		mazeContainer.setLayout(new GridBagLayout());
		mazeContainer.add(playerSprite, c_mazeContainer);
		mazeContainer.add(mazeTile, c_mazeContainer);
		mazeContainer.requestFocusInWindow();
		this.mazeGame = mazeContainer;

		return mazeContainer;
	}

	/**
	 * Retrieves the exit coordinates for the maze.
	 * 
	 * @return The exit coordinates.
	 */
	public Point2D getExit() {
		return mazeGen.getExitCoords();
	}
	
	// #########################################################
	// #                	PRIVATE METHODS            		   #
	// #########################################################
	
	/**
	 * Creates a JPanel filled with graphical maze Tile.
	 * 
	 * @return JPanel containing the maze Tile.
	 */
	private JPanel makeMazeTile() {
		JPanel mazeTile = new JPanel();
		mazeTile.setLayout(new GridBagLayout());
		GridBagConstraints c_mazeTile = new GridBagConstraints();
		int yPos = 0;
		JLabel tile = null;

		for (int col = 0; col < NUM_ROW; col++) {
			c_mazeTile.gridy = yPos;

			for (int row = 0; row < NUM_COL; row++) {

				// *** PATH Tile
				if (mazeData[col][row] == Tile.PATH_TILE.getValue()) {
					if (spriteToUse.equals(Sprite.PLAYER1.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.FOREST_PATH
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER2.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.CAVE_PATH
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER3.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.GRASS_PATH
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER4.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.WATER_PATH
										.getImage())));
					}

					// *** WALL Tile
				} else if (mazeData[col][row] == Tile.WALL_TILE.getValue()) {
					if (spriteToUse.equals(Sprite.PLAYER1.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.FOREST_WALL
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER2.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.CAVE_WALL
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER3.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.GRASS_WALL
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER4.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.WATER_WALL
										.getImage())));
					}

					// ** ENTRY Tile
				} else if (mazeData[col][row] == Tile.ENTRY_TILE.getValue()) {
					if (spriteToUse.equals(Sprite.PLAYER1.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.FOREST_ENTRY
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER2.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.CAVE_ENTRY
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER3.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.GRASS_ENTRY
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER4.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.WATER_ENTRY
												.getImage())));
					}

					// ** EXIT TILE
				} else if (mazeData[col][row] == Tile.EXIT_TILE.getValue()) {
					if (spriteToUse.equals(Sprite.PLAYER1.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.FOREST_EXIT
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER2.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.CAVE_EXIT
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER3.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.GRASS_EXIT
										.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER4.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class.getResource(MazeTheme.WATER_EXIT
										.getImage())));
					}

					// ** OBSTACLE
				} else if (mazeData[col][row] == Tile.OBSTACLE_TILE.getValue()) {
					if (spriteToUse.equals(Sprite.PLAYER1.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.FOREST_OBSTACLE
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER2.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.CAVE_OBSTACLE
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER3.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.GRASS_OBSTACLE
												.getImage())));

					} else if (spriteToUse.equals(Sprite.PLAYER4.getImage())) {
						tile = new JLabel(new ImageIcon(
								MazeData.class
										.getResource(MazeTheme.WATER_OBSTACLE
												.getImage())));
					}
				}// END_ELSE_IF

				mazeTile.add(tile, c_mazeTile);
			}// END_FOR

			yPos++;
		}// END_FOR

		return mazeTile;
	}

	/**
	 * Gives a component keybinding support to move within maze. 
	 * In this instance its WASD keyboard and arrows keys. 
	 * 
	 * @param component Component to be be provided keybinding support
	 * @param mazeStructure 2D Array of Integers that defines a maze
	 * @param startX The X coordinate of the entry point in the 2D maze array
	 * @param startY The Y coordinate of the entry point in the 2D maze array
	 */
	private static void addMotionSupport(JComponent component,
			int[][] mazeStructure, int startX, int startY) {
		SpriteMovement motion = new SpriteMovement(component,
				mazeStructure);
		int delta = 16;
		motion.addAction("LEFT", -delta, 0);
		motion.addAction("RIGHT", delta, 0);
		motion.addAction("UP", 0, -delta);
		motion.addAction("DOWN", 0, delta);
		motion.addAction("A", -delta, 0);
		motion.addAction("D", delta, 0);
		motion.addAction("W", 0, -delta);
		motion.addAction("S", 0, delta);
	}
	
	// #########################################################
	// # 					PRIVATE FIELDS 					   #
	// #########################################################
	
	private String spriteToUse;
	private JLayeredPane mazeGame;
	private MazeGenerator mazeGen;
	private int[][] mazeData;
	private final int NUM_COL;
	private final int NUM_ROW;
}