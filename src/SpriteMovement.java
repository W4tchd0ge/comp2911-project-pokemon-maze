import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * Adds keybinding for arrows keys and WASD to a component. There is one nested
 * class inside this SpriteMovement class.
 * @author Stefanus
 */
public class SpriteMovement {

	// #########################################################
	// #                     CONSTRUCTOR                       #
	// #########################################################

	/**
	 * Constructs a mechanism allowing a player sprite to move over a grid
	 * structure using keyboard input.
	 * 
	 * @param component
	 *            The component/sprite to be transformed over the grid.
	 * @param mazeStructure
	 *            The grid/maze structure to transform to.
	 */
	public SpriteMovement(JComponent component, int[][] mazeStructure) {
		this.component = component;
		this.mazeInfo = mazeStructure;
	}

	// #########################################################
	// #                  PRIVATE NESTED CLASS                 #
	// #########################################################

	/**
	 * The constructor for the movement base on the key that is being input in
	 * the MazeData class. This is a nested class inside a class.
	 */
	@SuppressWarnings("serial")
	private class MotionAction extends AbstractAction implements ActionListener {
		private int deltaX;
		private int deltaY;

		// NESTED CLASS CONSTRUCTOR.
		public MotionAction(String name, int deltaX, int deltaY) {
			super(name);
			this.deltaX = deltaX;
			this.deltaY = deltaY;
		}

		// NESTED CLASS METHOD.
		public void actionPerformed(ActionEvent e) {
			move(deltaX, deltaY);
		}
	}

	// #########################################################
	// #                    PUBLIC METHODS                     #
	// #########################################################

	/**
	 * This is to add the specific move that is created in the MazeData and it
	 * will then be passed on via MotionAction.
	 * 
	 * @param name
	 *            This will be the name of the keyboard stroke, e.g UP,
	 *            DOWN,LEFT, RIGHT, W, A, S, D
	 * @param deltaX
	 *            This will be the X movement that going to be affected.
	 * @param deltaY
	 *            This will be the Y movement that is going to be affected.
	 * @return It will return a MotionAction class.
	 */
	public MotionAction addAction(String name, int deltaX, int deltaY) {
		MotionAction action = new MotionAction(name, deltaX, deltaY);

		KeyStroke pressedKeyStroke = KeyStroke.getKeyStroke(name);

		InputMap inputMap = component
				.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(pressedKeyStroke, name);
		component.getActionMap().put(name, action);

		return action;
	}

	/**
	 * This will be the method that actually move the component to the new
	 * location. The component will stop moving when it reaches the bounds of
	 * its container.
	 * 
	 * @param deltaX
	 *            This will be the either addition or zero depending on the move
	 *            that is chosen and we have defined that in MazeData.
	 * @param deltaY
	 *            This will be the either addition or zero depending on the move
	 *            that is chosen and we have defined that in MazeData.
	 */
	public void move(int deltaX, int deltaY) {
		int componentWidth = component.getSize().width;
		int componentHeight = component.getSize().height;

		Dimension parentSize = component.getParent().getSize();
		int parentWidth = parentSize.width;
		int parentHeight = parentSize.height;

		// TO CALCULATE THE NEXT LOCATION OF X
		int nextX = Math.max(component.getLocation().x + deltaX, 0);

		if (nextX + componentWidth > parentWidth) {
			nextX = parentWidth - componentWidth;
		}

		// TO CALCULATE THE NEXT LOCATION OF Y
		int nextY = Math.max(component.getLocation().y + deltaY, 0);

		if (nextY + componentHeight > parentHeight) {
			nextY = parentHeight - componentHeight;
		}

		// TO CHECK WHETHER THE MOVEMENT IS LEGAL OR NOT, IF IT IS THEN MOVE IT.
		if (isLegal(nextX / 16, nextY / 16)) {
			component.setLocation(nextX, nextY);
		}
	}

	/**
	 * To get the information from the the maze data whether its a wall or path
	 * or entry or exit.
	 * 
	 * @param x
	 *            This is for the x input for the 2D array
	 * @param y
	 *            This is for the y input for the 2D array
	 * @return This will return an integer of the value that is stored in the 2D
	 *         array. Such as 1 for path, 2 for wall, 3 for entry point, 4 for
	 *         exit.
	 */
	public int getContent(int x, int y) {
		return this.mazeInfo[y][x];
	}

	// #########################################################
	// #                    PRIVATE METHODS                    #
	// #########################################################

	/**
	 * To check whether the move that a player want to make is it legal or not.
	 * 
	 * @param x
	 *            It takes in the next value of either x or y to compute it.
	 * @param y
	 *            It takes in the next value of either x or y to compute it.
	 * @return It returns a status either true or false.
	 */
	private boolean isLegal(int x, int y) {
		if (getContent(x, y) == Tile.WALL_TILE.getValue()
		||  getContent(x, y) == Tile.OBSTACLE_TILE.getValue()) {
			return false;
		} else {
			return true;
		}
	}

	// #########################################################
	// #                     PRIVATE FIELDS                    #
	// #########################################################

	private int[][] mazeInfo;
	private JComponent component;

}
