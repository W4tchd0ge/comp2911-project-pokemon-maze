import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The GameEngine class is responsible for aggregating all components of the
 * systems. It handles all requests made by the GUIs it creates.
 * @author JJ
 */
@SuppressWarnings("serial")
public class GameEngine extends JFrame {

	/**
	 * Runs an instance of the game engine. 
	 * @param args
	 */
	public static void main(String[] args) {
		GameEngine temp = new GameEngine();
		temp.run();
	}
	
	// #########################################################
	// #                	CONSTRUCTOR                        #
	// #########################################################
	
	/**
	 * Instantiates all GUIs except for the Maze GUI, and databases to be used
	 * by the GameEngine.
	 */
	public GameEngine() {

		// SET UP THE FRAME
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1040, 544); // SIZE OF JFRAME
		this.setTitle(" P O K E M A Z E "); // TITLE FOR JFRAME
		this.setResizable(false);
	    this.setVisible(true);

		// CREATE ALL THE GUIS
		this.menuGui = new GUIMenu();
		this.aboutGui = new GUIAbout();
		this.mazeGui = null; // MAZE GUI IS CREATED ON REQUEST OF NEW GAME
		this.endGameGui = new GUIEndGame();
		this.guiTraffic = new GuiStatus();
		this.config = new Configuration();
		this.mazeInfo = null;
		this.guis = new HashMap<String, GuiHandle>();

		// MAP THE GUIS TO THEIR RESPECTIVE ENUM
		guis.put(Request.MENU_GUI.getName(), menuGui);
		guis.put(Request.ABOUT_GUI.getName(), aboutGui);
		guis.put(Request.MAZE_GUI.getName(), mazeGui);
		guis.put(Request.END_GAME_GUI.getName(), endGameGui);

		// SET UP CONFIG DATABASE BY EXTRACTING DEFAULT SETTINGS FROM 
		// MENU GUI
		config.setPlayerName(menuGui.getName());
		config.setDifficulty(menuGui.getDifficulty());
		config.setSprite(menuGui.getSprite());

		// DISPLAY THE MENU GUI
		this.getContentPane().add(menuGui);
		this.setVisible(true);
		
		// SET UP THE BGM
		bgm = makeBGM(Music.BGM.getMusic());
		bgm.loop(Clip.LOOP_CONTINUOUSLY);
		muteControl = (BooleanControl) bgm.getControl(BooleanControl.Type.MUTE);
	}

	// #########################################################
	// #                	PUBLIC METHODS                     #
	// #########################################################
	
	/**
	 * Run the game and polls each GUI for any requests and executes them.
	 */
	public void run() {
		while (true) {
			
			// HANDLE A CHANGE DISPLAY REQUEST
			if (!guiTraffic.getRequest().equals(Request.NONE.getName())
					&& !guiTraffic.getRequest().equals(Request.RESET.getName())
					&& !guiTraffic.getRequest().equals(Request.MUTE.getName())) {

				changeDisplay(guiTraffic.getRequest());
				guiTraffic.setRequest(Request.NONE.getName());

			// HANDLE RESET REQUEST
			} else if (guiTraffic.getRequest().equals(Request.RESET.getName())) {

				mazeGui = new GUIMaze(mazeInfo.makeMazeGame(),	// WE RECREATE THE MAZE FROM SCRATCH WITH THE SAME
						config.getPlayerName());				// PARAMETERS TO GET THE MAZE AS IT WAS INITIALLY
				guis.put(Request.MAZE_GUI.getName(), mazeGui);	// BEFORE THE PLAYER STARTED

				display(mazeGui);
				
				mazeGui.setGUIRequest(Request.NONE.getName());
				guiTraffic.setRequest(Request.NONE.getName());

			// HANDLE GAME FINISHED REQUEST
			} else if (mazeGui != null
					&& mazeInfo.getOrigiMaze().getComponent(FRAME_START)
							.getLocation().getX() == mazeInfo.getExit().getX()
							* PIXEL_WIDTH
					&& mazeInfo.getOrigiMaze().getComponent(FRAME_START)
							.getLocation().getY() == mazeInfo.getExit().getY()
							* PIXEL_WIDTH) {
				guiTraffic.setRequest(Request.END_GAME_GUI.getName());

			// HANDLE MUTE REQUEST
			} else if (guiTraffic.getRequest().equals(Request.MUTE.getName())) {
				if (muteControl.getValue()) {
					muteControl.setValue(false); // TRUE TO MUTE, FALSE TO UNMUTE
				} else {
					muteControl.setValue(true);
				}
				guiTraffic.setRequest(Request.NONE.getName());
				guis.get(guiTraffic.currentlyActive()).setGUIRequest(
						Request.NONE.getName());
			
			// PULL ANY REQUESTS FROM ACTIVE GUI
			} else {
				guiTraffic.setRequest(guis.get(guiTraffic.currentlyActive())
						.getGUIRequest());
			}
		}// END_WHILE
	}

	// #########################################################
	// #                	PRIVATE METHODS            		   #
	// #########################################################
	
	/**
	 * Changes which GUI is to be displayed in the frame.
	 * 
	 * @param req
	 *            A string that specifies a request to change to a different GUI.
	 */
	private void changeDisplay(String req) {
		
		if (req.equals(Request.MENU_GUI.getName())) {	// DISPLAY MAIN MENU

			display(menuGui);
			setReqAndActive(menuGui, req);

		} else if (req.equals(Request.ABOUT_GUI.getName())) { // DISPLAY HELP GUI

			display(aboutGui);
			setReqAndActive(aboutGui, req);

		} else if (req.equals(Request.MAZE_GUI.getName())) { // DISPLAY MAZE GUI

			config.setPlayerName(menuGui.getPlayerName());	// RETRIEVE AND STORE PLAYER SETTINGS
			config.setDifficulty(menuGui.getDifficulty());
			config.setSprite(menuGui.getSprite());

			mazeInfo = new MazeData(config.getSprite(), config.getDifficulty());
			mazeGui = new GUIMaze(mazeInfo.getOrigiMaze(),	// MAKE NEW MAZE WITH PLAYER SETTINGS
					config.getPlayerName());
			
			guis.put(Request.MAZE_GUI.getName(), mazeGui);
			display(mazeGui);
			setReqAndActive(mazeGui, req);
			
		} else if (req.equals(Request.END_GAME_GUI.getName())) { // DISPLAY END GAME GUI
			
			mazeGui = null;
			display(endGameGui);
			setReqAndActive(endGameGui, req);
		}
	}

	/**
	 * Create BGM streaming objects.
	 * 
	 * @param dir
	 *            String that holds the location of the music file to be played.
	 * @return The Clip object that will stream the requested music file.
	 */
	private Clip makeBGM(String dir) {
		Clip clip = null;
		try {
			// OPEN AN AUDIO INPUT STREAM.
			File soundFile = new File(dir);
			AudioInputStream audioIn = AudioSystem
					.getAudioInputStream(soundFile);
			// GET A SOUND CLIP RESOURCE.
			clip = AudioSystem.getClip();
			// OPEN AUDIO CLIPE AND LOAD SAMPLES FROM THE AUDIO INPUT STREAM
			clip.open(audioIn);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		return clip;
	}

	/**
	 * Removes existing panels in the JFrame and displays the requested GUI.
	 * 
	 * @param gui
	 *            GUI requested to be displayed.
	 */
	private void display(JPanel gui) {
		this.getContentPane().removeAll();
		this.getContentPane().add(gui);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}

	/**
	 * Resets the request made by the GUI and sets which GUI is now currently
	 * active after the request.
	 * 
	 * @param gui
	 *            GUI that made the request.
	 * @param guiReqd
	 *            GUI that was requested to be displayed.
	 */
	private void setReqAndActive(GuiHandle gui, String guiReqd) {
		gui.setGUIRequest(Request.NONE.getName());
		guiTraffic.setCurrentlyActive(guiReqd);
	}

	// #########################################################
	// #                	PRIVATE FIELDS                     #
	// #########################################################
	
	private GUIMenu menuGui;
	private GUIAbout aboutGui;
	private GUIMaze mazeGui;
	private GUIEndGame endGameGui;
	private GuiStatus guiTraffic;
	private Configuration config;
	private MazeData mazeInfo;
	private HashMap<String, GuiHandle> guis;
	private Clip bgm;
	private BooleanControl muteControl;

	// #########################################################
	// #                		CONSTANTS                      #
	// #########################################################
	
	private static final int PIXEL_WIDTH = 16;
	private static final int FRAME_START = 0;

}
