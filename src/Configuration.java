/**
 * A database for a parent GameEngine object.
 * @author JJ
 */
public class Configuration {

	// #########################################################
	// #                	PUBLIC METHODS                     #
	// #########################################################
	
	/**
	 * Stores which sprite the player has chosen.
	 * 
	 * @param sprite
	 *            A string that specifies a sprite.
	 */
	public void setSprite(String sprite) {
		this.sprite = sprite;
	}

	/**
	 * Gets the sprite the player chose.
	 * 
	 * @return A string that specifies a sprite.
	 */
	public String getSprite() {
		return sprite;
	}

	/**
	 * Stores the player name.
	 * 
	 * @param name
	 *            A string that specifies a player's name.
	 */
	public void setPlayerName(String name) {
		this.playerName = name;
	}

	/**
	 * Gets the player's name.
	 * 
	 * @return A string that specifies a player's name.
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * Stores the chosen maze difficulty level.
	 * 
	 * @param diffLvl
	 *            An integer specifying the difficulty of the maze.
	 */
	public void setDifficulty(int diffLvl) {
		this.gameDifficulty = diffLvl;
	}

	/**
	 * Gets the chosen maze difficulty level.
	 * 
	 * @return An integer specifying the difficulty of the maze.
	 */
	public int getDifficulty() {
		return gameDifficulty;
	}
	
	// #########################################################
	// #                	PRIVATE FIELDS                     #
	// #########################################################

	private String sprite;
	private String playerName;
	private int gameDifficulty;
	
}
