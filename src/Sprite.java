/**
 * Constants used to define sprites used for players.
 * @author SBORG
 */
public enum Sprite {

	PLAYER1("/img/p1.png"), 
	PLAYER2("/img/p2.png"), 
	PLAYER3("/img/p3.png"), 
	PLAYER4("/img/p4.png");

	private String imageDirectory;

	private Sprite(String directory) {
		this.imageDirectory = directory;
	}

	public String getImage() {
		return imageDirectory;
	}

}
