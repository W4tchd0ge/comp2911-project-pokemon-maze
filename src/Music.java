/**
 * Constants defining music directories.
 * @author JJ
 */
public enum Music {
	
	BGM("src/music/bgm.aiff");
	
	private String directory;
	
	private Music(String dir){
		this.directory = dir;
	}
	
	public String getMusic(){
		return this.directory;
	}

}
