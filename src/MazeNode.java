/**
 * Used for the BFS path queue.
 * @author Pokemon_Mike
 */
public class MazeNode {

	// #########################################################
	// # 					CONSTRUCTOR 					   #
	// #########################################################

	/**
	 * Used for the queue.
	 * 
	 * @param parentRow
	 *            row of the node it came from
	 * @param parentCol
	 *            column of the node it came from
	 * @param currentRow
	 *            row of where the node is
	 * @param currentCol
	 *            column of where the node is
	 */
	public MazeNode(int parentRow, int parentCol, int currentRow, int currentCol) {
		this.parentRow = parentRow;
		this.parentCol = parentCol;
		this.currentRow = currentRow;
		this.currentCol = currentCol;
	}

	// #########################################################
	// # 					PUBLIC METHODS 					   #
	// #########################################################

	/**
	 * Get the row of the parent node
	 * 
	 * @return row of the parent node
	 */
	public int getParentRow() {
		return this.parentRow;
	}

	/**
	 * Get the column of the parent node
	 * 
	 * @return column of the parent node
	 */
	public int getParentCol() {
		return this.parentCol;
	}

	/**
	 * Get the row of the current node
	 * 
	 * @return row of the current node
	 */
	public int getCurrentRow() {
		return this.currentRow;
	}

	/**
	 * Get the column of the current node
	 * 
	 * @return column on the current node
	 */
	public int getCurrentCol() {
		return this.currentCol;
	}

	// #########################################################
	// # 					PRIVATE FIELDS					   #
	// #########################################################

	private int parentRow;
	private int parentCol;
	private int currentRow;
	private int currentCol;

}
