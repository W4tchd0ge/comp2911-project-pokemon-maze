import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * GUI used for the maze game menu.
 * @author SBORG
 */
@SuppressWarnings("serial")
public class GUIMenu extends JPanel implements GuiHandle {

	// #########################################################
	// #                      CONSTRUCTOR                      #
	// #########################################################
	
	/**
	 * Constructs an instance of the menu GUI.
	 */
	public GUIMenu() {
		// ************************* PRIVATE FIELDS **************************//
		// SETTING GAME SETTING DEFAULTS
		this.request = Request.NONE.getName();
		this.sprite = Sprite.PLAYER1.getImage();
		this.playerName = DEFAULT_PLAYER_NAME;
		this.difficulty = 1;

		// **************************** BANNER *******************************//
		JLabel bannerImg = new JLabel(new ImageIcon(
				GUIMenu.class.getResource("/img/main-banner.png")));
		JPanel banner = new JPanel();
		banner.setLayout(new GridBagLayout());
		banner.add(bannerImg);

		// ************************ PLAYER Sprite ***************************//
		// SPRITE OPTION 1
		JButton p1 = new JButton();
		p1.setIcon(new ImageIcon(GUIMenu.class.getResource(Sprite.PLAYER1
				.getImage())));
		p1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sprite = Sprite.PLAYER1.getImage();
			}
		});

		// SPRITE OPTION 2
		JButton p2 = new JButton();
		p2.setIcon(new ImageIcon(GUIMenu.class.getResource(Sprite.PLAYER2
				.getImage())));
		p2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sprite = Sprite.PLAYER2.getImage();
			}
		});

		// SPRITE OPTION 3
		JButton p3 = new JButton();
		p3.setIcon(new ImageIcon(GUIMenu.class.getResource(Sprite.PLAYER3
				.getImage())));
		p3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sprite = Sprite.PLAYER3.getImage();
			}
		});

		// SPRITE OPTION 4
		JButton p4 = new JButton();
		p4.setIcon(new ImageIcon(GUIMenu.class.getResource(Sprite.PLAYER4
				.getImage())));
		p4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sprite = Sprite.PLAYER4.getImage();
			}
		});

		// COMBINE ALL SPRITE OPTIONS INTO ONE PANEL
		JPanel playerSprite = new JPanel();
		playerSprite.setBackground(new Color(113, 42, 35));
		GridBagLayout l_playerSprite = new GridBagLayout();
		l_playerSprite.columnWidths = new int[] { 334, 85, 85, 85, 85, 334 };
		l_playerSprite.rowHeights = new int[] { 60 };
		playerSprite.setLayout(l_playerSprite);

		GridBagConstraints c_playerSprite = new GridBagConstraints();
		c_playerSprite.anchor = GridBagConstraints.CENTER;
		c_playerSprite.gridx = 1;
		playerSprite.add(p1, c_playerSprite);
		c_playerSprite.gridx = 2;
		playerSprite.add(p2, c_playerSprite);
		c_playerSprite.gridx = 3;
		playerSprite.add(p3, c_playerSprite);
		c_playerSprite.gridx = 4;
		playerSprite.add(p4, c_playerSprite);

		// ************************* PLAYER INFO *****************************//
		// PLAYER NAME
		JLabel playerNameLabel = new JLabel("Player Name:");
		playerNameLabel.setForeground(new Color(252,193,173));
		
		// PLAYER NAME INPUT
		final JTextField nameInput = new JTextField();
		nameInput.setColumns(9);
		nameInput.setText(DEFAULT_PLAYER_NAME);

		// GAME LEVEL
		JLabel gameLevel = new JLabel("Game Level:");
		gameLevel.setForeground(new Color(252,193,173));

		// GAME LEVEL INPUT
		final JSpinner levelSelector = new JSpinner(new SpinnerNumberModel(1,
				1, 3, 1));
		levelSelector.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				difficulty = (Integer) levelSelector.getValue();
			}
		});

		// COMBINE ALL PLAYER INFO INTO ONE PANEL
		JPanel playerInfo = new JPanel();
		playerInfo.setBackground(new Color(113, 42, 35));
		GridBagConstraints c_playerInfo = new GridBagConstraints();
		GridBagLayout l_playerInfo = new GridBagLayout();
		l_playerInfo.columnWidths = new int[] { 504, 504 };
		l_playerInfo.rowHeights = new int[] { 40, 40, 20 };
		playerInfo.setLayout(l_playerInfo);

		c_playerInfo.gridx = 0;
		c_playerInfo.gridy = 0;
		c_playerInfo.anchor = GridBagConstraints.LINE_END;
		playerInfo.add(playerNameLabel, c_playerInfo);

		c_playerInfo.gridx = 1;
		c_playerInfo.gridy = 0;
		c_playerInfo.anchor = GridBagConstraints.LINE_START;
		playerInfo.add(nameInput, c_playerInfo);

		c_playerInfo.gridx = 0;
		c_playerInfo.gridy = 1;
		c_playerInfo.anchor = GridBagConstraints.LINE_END;
		playerInfo.add(gameLevel, c_playerInfo);

		c_playerInfo.gridx = 1;
		c_playerInfo.gridy = 1;
		c_playerInfo.anchor = GridBagConstraints.LINE_START;
		playerInfo.add(levelSelector, c_playerInfo);

		// ************************* PLAY BUTTONS ****************************//
		// SETTINGS BUTTON
		JButton aboutButton = new JButton();
		aboutButton.setIcon(new ImageIcon(GUIMenu.class
				.getResource("/img/button-about.png")));
		aboutButton.setFocusable(false);
		aboutButton.setOpaque(false);
		aboutButton.setContentAreaFilled(false);
		aboutButton.setBorderPainted(false);
		aboutButton.setFocusPainted(false);
		aboutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				request = Request.ABOUT_GUI.getName();
			}
		});

		// MUSIC BUTTON
		JButton musicButton = new JButton();
		musicButton.setIcon(new ImageIcon(GUIMenu.class
				.getResource("/img/button-music.png")));
		musicButton.setFocusable(false);
		musicButton.setOpaque(false);
		musicButton.setContentAreaFilled(false);
		musicButton.setBorderPainted(false);
		musicButton.setFocusPainted(false);
		musicButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setGUIRequest(Request.MUTE.getName());
			}
		});

		// NEW-GAME BUTTON
		JButton newGameButton = new JButton();
		newGameButton.setIcon(new ImageIcon(GUIMenu.class
				.getResource("/img/button-newgame.png")));
		newGameButton.setFocusable(false);
		newGameButton.setOpaque(false);
		newGameButton.setContentAreaFilled(false);
		newGameButton.setBorderPainted(false);
		newGameButton.setFocusPainted(false);
		newGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				// SET THE PLAYER NAME, IF USER HAS SPECIFIED ONE
				if (!nameInput.getText().equals("")) {
					if (nameInput.getText().length() >= 15) {
						nameInput.setText(nameInput.getText().substring(0, 15));
					}
					playerName = nameInput.getText();
				}

				// BEGIN THE MAZE GAME
				request = Request.MAZE_GUI.getName();
			}
		});

		// COMBINE ALL THE BUTTONS INTO ONE PANEL
		JPanel playButtons = new JPanel();
		playButtons.setBackground(new Color(113, 42, 35));
		GridBagLayout l_playButtons = new GridBagLayout();
		l_playButtons.columnWidths = new int[] { 336, 336, 336 };
		playButtons.setLayout(l_playButtons);

		GridBagConstraints c_playButtons = new GridBagConstraints();
		c_playButtons.gridx = 0;
		c_playButtons.gridy = 0;
		c_playButtons.insets = new Insets(0, 8, 8, 0);
		c_playButtons.anchor = GridBagConstraints.LINE_START;
		playButtons.add(aboutButton, c_playButtons);

		c_playButtons.gridx = 1;
		c_playButtons.gridy = 0;
		c_playButtons.insets = new Insets(0, 0, 0, 0);
		c_playButtons.anchor = GridBagConstraints.CENTER;
		playButtons.add(musicButton, c_playButtons);

		c_playButtons.gridx = 2;
		c_playButtons.gridy = 0;
		c_playButtons.insets = new Insets(0, 0, 8, 8);
		c_playButtons.anchor = GridBagConstraints.LINE_END;
		playButtons.add(newGameButton, c_playButtons);

		// ******************** SUBPANELS TO MAINFRAME ***********************//
		// CONFIGURE GUI LAYOUT
		this.setBackground(new Color(74, 43, 42));
		GridBagLayout gbl_main = new GridBagLayout();
		gbl_main.columnWidths = new int[] { 1040 };
		gbl_main.rowHeights = new int[] { 16, 280, 0, 0, 0, 16 };
		this.setLayout(gbl_main);

		// ADD MENU COMPONENTS TO GUI
		GridBagConstraints c_main = new GridBagConstraints();
		c_main.anchor = GridBagConstraints.NORTH;
		c_main.gridy = 1;
		this.add(bannerImg, c_main);
		c_main.gridy = 2;
		this.add(playerSprite, c_main);
		c_main.gridy = 3;
		this.add(playerInfo, c_main);
		c_main.gridy = 4;
		this.add(playButtons, c_main);
	}

	// #########################################################
	// # 					PUBLIC METHODS  				   #
	// #########################################################
	
	/**
	 * Used by game engine to retrieve the visibility status of this JPanel.
	 * 
	 * @return The visiblity status.
	 */
	public String getGUIRequest() {
		return request;
	}
	
	/**
	 * Used by game engine to alter the visibility of this JPanel.
	 * 
	 * @param request
	 *            The new visibility setting for this GUI.
	 */
	public void setGUIRequest(String request) {
		this.request = request;
	}

	/**
	 * Retrieves the selected player sprite.
	 * 
	 * @return The player sprite.
	 */
	public String getSprite() {
		return this.sprite;
	}

	/**
	 * Retrieves the players name.
	 * 
	 * @return The The player name.
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * Retrieves the difficulty of the maze.
	 * 
	 * @return The difficulty level.
	 */
	public int getDifficulty() {
		return this.difficulty;
	}

	// #########################################################
	// # 					PRIVATE FIELDS 					   #
	// #########################################################

	private final String DEFAULT_PLAYER_NAME = "PLAYER NAME";
	private String request;
	private String sprite;
	private String playerName;
	private int difficulty;
	
}
