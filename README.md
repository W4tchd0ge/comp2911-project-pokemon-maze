### What is this repository for? ###

This repository is for holding all versions of the Java Poke-maze game developed by the team Squad Primus, for the course COMP2911 14s1 at UNSW as part of their final project.

### Who do I talk to? ###

* Team Lead/ Engine Creator : JJ Lau
* Designer : Samantha Borg
* Animation : Stefanus Zhang
* Maze Generator : Mike Simarta